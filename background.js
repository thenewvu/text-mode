/* global chrome */

const storage = chrome.storage
const onBeforeRequest = chrome.webRequest.onBeforeRequest
const onCompleted = chrome.webRequest.onCompleted

const defaultConf = {
  blockImages: true,
  blockFonts: true,
  blockObjects: true,
  blockMedia: true,
  whitelist: []
}

let _conf = {}
const blankImg = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAQAAAAmTKJWAAAAMElEQVR42u3NMQEAAAgDINc/rQm0xS4oQPamJjKZTCaTyWQymUwmk8lkMplMJpM1PEYyaNegAvkCAAAAAElFTkSuQmCC'
const skippedRequestMap = {}

const onRequestCompleted = (detail) => {
  if (skippedRequestMap[detail.requestId]) {
    delete skippedRequestMap[detail.requestId]
  }
}

const onBeforeRequestImage = (detail) => {
  if (skippedRequestMap[detail.requestId]) {
    return
  }

  if (detail.url.endsWith('?reload')) {
    skippedRequestMap[detail.requestId] = true
    return {
      redirectUrl: detail.url.slice(0, -7)
    }
  }

  for (const item of _conf.whitelist) {
    if (item && detail.url.startsWith(item)) {
      return
    }
  }

  return {
    redirectUrl: blankImg
  }
}

const onBeforeRequestObject = (detail) => {
  let whitelisted = false
  for (const item of _conf.whitelist) {
    if (item && detail.url.startsWith(item)) {
      whitelisted = true
      break
    }
  }
  return {
    cancel: !whitelisted
  }
}

const onBeforeRequestMedia = (detail) => {
  let whitelisted = false
  for (const item of _conf.whitelist) {
    if (item && detail.url.startsWith(item)) {
      whitelisted = true
      break
    }
  }
  return {
    cancel: !whitelisted
  }
}

const onBeforeRequestFont = (detail) => {
  let whitelisted = false
  for (const item of _conf.whitelist) {
    if (item && detail.url.startsWith(item)) {
      whitelisted = true
      break
    }
  }
  return {
    cancel: !whitelisted
  }
}

function onConfChanged () {
  storage.sync.get(defaultConf, (conf) => {
    if (conf === defaultConf) {
      storage.sync.set(conf)
    }

    _conf = conf

    if (conf.blockImages) {
      onBeforeRequest.addListener(
        // listener
        onBeforeRequestImage,
        // filters
        {
          urls: ['http://*/*', 'https://*/*'],
          types: ['image']
        },
        // extraInfoSpec
        ['blocking']
      )
    } else {
      onBeforeRequest.removeListener(
        onBeforeRequestImage
      )
    }

    if (conf.blockFonts) {
      onBeforeRequest.addListener(
        // listener
        onBeforeRequestFont,
        // filters
        {
          urls: ['http://*/*', 'https://*/*'],
          types: ['font']
        },
        // extraInfoSpec
        ['blocking']
      )
    } else {
      onBeforeRequest.removeListener(
        onBeforeRequestFont
      )
    }

    if (conf.blockObjects) {
      onBeforeRequest.addListener(
        // listener
        onBeforeRequestObject,
        // filters
        {
          urls: ['http://*/*', 'https://*/*'],
          types: ['object']
        },
        // extraInfoSpec
        ['blocking']
      )
    } else {
      onBeforeRequest.removeListener(
        onBeforeRequestObject
      )
    }

    if (conf.blockMedia) {
      onBeforeRequest.addListener(
        // listener
        onBeforeRequestMedia,
        // filters
        {
          urls: ['http://*/*', 'https://*/*'],
          types: ['media']
        },
        // extraInfoSpec
        ['blocking']
      )
    } else {
      onBeforeRequest.removeListener(
        onBeforeRequestMedia
      )
    }
  })
}

storage.onChanged.addListener(onConfChanged)

onConfChanged()

onCompleted.addListener(
  // listener
  onRequestCompleted,
  // filters
  {
    urls: ['http://*/*', 'https://*/*'],
    types: ['image']
  }
)
