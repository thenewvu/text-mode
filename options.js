/* global chrome */

function $ (id) {
  return document.getElementById(id)
}

function notify (text) {
  const $msg = $('msg')
  $msg.textContent = text
  setTimeout(() => { $msg.textContent = '' }, 750)
}

function save () {
  const opts = {
    blockFonts: $('block-fonts').checked,
    blockImages: $('block-images').checked,
    blockObjects: $('block-objects').checked,
    blockMedia: $('block-media').checked,
    whitelist: $('whitelist').value.split(',')
  }
  chrome.storage.sync.set(opts, () => {
    notify('Options saved.')
  })
}

function load () {
  chrome.storage.sync.get(null, (opts) => {
    const $blockFonts = $('block-fonts')
    const $blockImages = $('block-images')
    const $blockObjects = $('block-objects')
    const $blockMedia = $('block-media')
    const $whitelist = $('whitelist')

    $blockFonts.checked = opts.blockFonts
    $blockImages.checked = opts.blockImages
    $blockObjects.checked = opts.blockObjects
    $blockMedia.checked = opts.blockMedia
    if (Array.isArray(opts.whitelist)) {
      $whitelist.value = opts.whitelist.join(',')
    }

    $blockImages.onchange = save
    $blockFonts.onchange = save
    $blockObjects.onchange = save
    $blockMedia.onchange = save
    $whitelist.onchange = save
  })
}

document.addEventListener('DOMContentLoaded', load)
