function reloadImgSrc (ev) {
  ev.target.src = ev.target.src + '?reload'

  ev.target.removeEventListener('mouseenter', reloadImgSrc)

  ev.stopPropagation()
  ev.preventDefault()
}

for (const img of document.images) {
  if (img.src.startsWith('data:image')) {
    continue
  }

  img.addEventListener('mouseenter', reloadImgSrc)
}
